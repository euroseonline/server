package main

import (
	"database/sql"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

const (
	// ConnHost Host to bind to
	ConnHost = "127.0.0.1"
	// ConnPort Port to bind to
	ConnPort = "29000"
	// ConnType Type of connection
	ConnType = "tcp"
)

// User struct
type User struct {
	id          uint16
	username    string
	password    string
	accesslevel int32
	online      bool
	active      bool
}

// Channel struct
type Channel struct {
	id             uint16
	name           string
	connected      uint16
	maxconnections uint16
	host           string
	port           uint16
	lanip          string
	lansubmask     string
}

// Packet struct
type Packet struct {
	Size    uint16
	Command uint16
	Unused  uint16
	Buffer  []byte
}

// ToBuffer Create a valid buffer for transport over network
func (p Packet) ToBuffer() []byte {
	b := make([]byte, 6)
	binary.LittleEndian.PutUint16(b[0:], p.Size)
	binary.LittleEndian.PutUint16(b[2:], p.Command)
	binary.LittleEndian.PutUint16(b[4:], p.Unused)
	b = append(b, p.Buffer[:p.Size-6]...)
	// binary.LittleEndian.PutUint16(b[2:], 0x07d0)
	return b
}

var pak = Packet{Buffer: make([]byte, 1000)}

func startPacket(p Packet, c uint16, s uint16) {
	p.Command = c
	p.Size = s
}

func beginPacket(p *Packet, c uint16) {
	p.Command = c
	p.Size = 6
}

func addByte(p *Packet, v byte) {
	p.Buffer[p.Size-6] = v
	p.Size++
}

func addString(p *Packet, v string) {
	for i := 0; i < len(v); i++ {
		addByte(p, v[i])
	}
}

func addWord(p *Packet, v uint16) {
	// p.Buffer[p.Size-6] = v
	binary.LittleEndian.PutUint16(p.Buffer[p.Size-6:], v)
	// var h, l uint8 = uint8(v >> 8), uint8(v & 0xff)
	// p.Buffer[p.Size-6] = h
	// p.Buffer[p.Size+1-6] = l
	p.Size += 2
}

func addDWord(p *Packet, v uint32) {
	//binary.LittleEndian.PutUint32(p.Buffer, v)
	// p.Buffer[p.Size-6] = v
	binary.LittleEndian.PutUint32(p.Buffer[p.Size-6:], v)
	p.Size += 4
}

func handleConnection(conn net.Conn) {
	defer func() {
		log.Println("Closing connection...", conn.RemoteAddr())
		conn.Close()
	}()

	timeoutDuration := 1 * time.Second

	// Make a buffer to hold incoming data.
	buf := make([]byte, 1024)

	for {
		conn.SetReadDeadline(time.Now().Add(timeoutDuration))

		bufLen, err := conn.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
		}

		if bufLen < 1 {
			continue
		}

		log.Println("Received data of length:", bufLen)

		bufPointer := 0
		for bufPointer < bufLen {
			pakLen := int(buf[bufPointer])
			packet := buf
			log.Println("Received packet of length:", bufLen)
			log.Println("Packet:", packet)

			cryptPacket(&packet)
			log.Println("Packet:", packet)

			p := Packet{binary.LittleEndian.Uint16(packet[0:2]), binary.LittleEndian.Uint16(packet[2:4]), binary.LittleEndian.Uint16(packet[4:6]), packet[6:]}

			err := handlePacket(conn, p)
			if err != nil {
				log.Println("Packet was not handled?")
			}

			bufPointer += pakLen
		}
		// log.Println(buf)
	}

	// Send a response back to person contacting us.
	// conn.Write([]byte("Message received."))
}

func handlePacket(conn net.Conn, packet Packet) error {
	log.Println("p:", packet)
	log.Println("b:", string(packet.Buffer))
	log.Println(fmt.Sprintf("Command: %#v", packet.Command))

	if packet.Command == 0x703 {
		// pakEncryptionRequest
		beginPacket(&pak, 0x7ff)
		addWord(&pak, 0xaf02)
		addWord(&pak, 0xbd46)
		addWord(&pak, 0x0009)
		addWord(&pak, 0x0012)
		addWord(&pak, 0x0000)
		addDWord(&pak, 0xcdcdcdcd)
		addDWord(&pak, 0xcdcdcdcd)
		addDWord(&pak, 0xcdcdcdcd)
		addDWord(&pak, 0xcdcdcdcd)
		addWord(&pak, 0xcdcd)
		addByte(&pak, 0xd3)
		log.Println("Buffer:", fmt.Sprintf("%#v", pak.Buffer[:pak.Size-6]))
		log.Println("Sending packet (hex)", fmt.Sprintf("%#v", pak.ToBuffer()))
		// conn.Write([]byte(fmt.Sprintf("%v", pak))[:pak.Size])
		conn.Write(pak.ToBuffer())
	} else if packet.Command == 0x708 {
		// pakUserLogin
		var password = string(packet.Buffer[:32])
		var username string
		if packet.Size-6-32 > 16 {
			username = string(packet.Buffer[32:48])
		} else {
			username = string(packet.Buffer[32 : packet.Size-7])
		}
		log.Println("Username:", fmt.Sprintf("'%v'", username))
		log.Println("Password:", password)

		beginPacket(&pak, 0x708)

		// TODO: Check gameguard
		// addByte(&pak, 10)
		// addDWord(&pak, 0)
		// conn.Write(pak.ToBuffer())
		// conn.Close()

		row := db.QueryRow("SELECT id, password, accesslevel, online, active FROM accounts WHERE username=?", username)

		var user User
		if err := row.Scan(&user.id, &user.password, &user.accesslevel, &user.online, &user.active); err != nil {
			log.Println(err)

			addByte(&pak, 2)
			addDWord(&pak, 0)

			conn.Write(pak.ToBuffer())
			return conn.Close()
		}

		log.Println("DB user:", user.id, "-", user.password, "-", user.accesslevel, "-", user.online, "-", user.active)

		if password != user.password {
			addByte(&pak, 3)
			addDWord(&pak, 0)
			conn.Write(pak.ToBuffer())
			return conn.Close()
		}

		if !user.active {
			addByte(&pak, 9)
			addDWord(&pak, 0)
			conn.Write(pak.ToBuffer())
			return conn.Close()
		}

		if user.online {
			addByte(&pak, 4)
			addDWord(&pak, 0)
			conn.Write(pak.ToBuffer())

			// Disconnect user from other servers
			// Send this packet to all servers
			// var pak2 Packet
			// beginPacket(&pak2, 0x502)
			// addByte(&pak2, 1)
			// addDWord(&pak2, user.id)
			// TODO: run through cryptPacket
			// for (UINT i = 0; i < ServerList.size(); i++)
			// send(ServerList.at(i) -> sock, (char *) & pak2, pak2.Size, 0);

			db.Query("UPDATE accounts SET online=0 WHERE username=?", username)
			return conn.Close()
		}

		if user.accesslevel > 0 && user.accesslevel < 100 { //TODO: make minimumAccessLevel configurable
			// Maintenance Mode
			addByte(&pak, 0)
			addDWord(&pak, 0)
			conn.Write(pak.ToBuffer())
			return conn.Close()
		}

		if user.accesslevel < 1 {
			// Banned
			addByte(&pak, 5)
			addDWord(&pak, 0)
			conn.Write(pak.ToBuffer())
			return conn.Close()
		}

		// OK to log in
		rows, err := db.Query("SELECT id, name FROM channels WHERE type=1")
		if err != nil {
			log.Println("Fetching charservers failed")
			addByte(&pak, 7)
			addDWord(&pak, 0)
			conn.Write(pak.ToBuffer())
			return conn.Close()
		}

		count := 0
		for rows.Next() {
			count++
		}

		if count < 1 {
			log.Println("No charserver online")
			addByte(&pak, 7)
			addDWord(&pak, 0)
			conn.Write(pak.ToBuffer())
			return conn.Close()
		}

		addByte(&pak, 0x80)
		if user.accesslevel == 300 {
			// GM level 1
			addWord(&pak, 256)
		} else if user.accesslevel == 400 {
			// GM level 2
			addWord(&pak, 512)
		} else if user.accesslevel == 500 {
			// Admin
			addWord(&pak, 768)
		} else {
			// Regular user
			addWord(&pak, 100)
		}

		addWord(&pak, 0x0000)

		rows, _ = db.Query("SELECT id, name FROM channels WHERE type=1")
		for rows.Next() {
			var channel Channel
			err := rows.Scan(&channel.id, &channel.name)
			if err != nil {
				log.Println("Could not add Channel to list", err.Error())
				break
			}
			log.Println("Add channel to list", channel.id, 30+channel.id, channel.name)

			addByte(&pak, byte(30+channel.id))
			addString(&pak, channel.name)
			addByte(&pak, 0)
			addByte(&pak, byte(channel.id))
			addByte(&pak, 0)
			addWord(&pak, 0)
		}

		// TODO: add logged in status to socket

		conn.Write(pak.ToBuffer())

		// TODO: remove, this is login failed packet
		// addByte(&pak, 10)
		// addDWord(&pak, 0)
		// conn.Write(pak.ToBuffer())
		// conn.Close()
	} else if packet.Command == 0x704 {
		// pakGetServers

		// TODO: check if logged in

		var servernum = uint32(packet.Buffer[0])
		rows, err := db.Query("SELECT id,name,connected,maxconnections FROM channels WHERE id=? and type=2", servernum)
		if err != nil {
			log.Println("Could not get servers", err.Error())
			return conn.Close()
		}

		count := 0
		for rows.Next() {
			count++
		}

		if count < 1 {
			log.Println("Channel not found", fmt.Sprintf("(id: %v, type: 2)", servernum))
			return conn.Close()
		}

		beginPacket(&pak, 0x704)
		addDWord(&pak, servernum)
		addByte(&pak, byte(count))

		rows, _ = db.Query("SELECT id,name,connected,maxconnections FROM channels WHERE id=? and type=2", servernum)
		for rows.Next() {
			var channel Channel
			err := rows.Scan(&channel.id, &channel.name, &channel.connected, &channel.maxconnections)
			if err != nil {
				log.Println("Could not add channel", err.Error())
				return conn.Close()
			}
			status := (channel.connected * 100) / channel.maxconnections
			addWord(&pak, channel.id)
			addByte(&pak, 0)
			addWord(&pak, status)
			addString(&pak, channel.name)
			addByte(&pak, 0)
		}

		conn.Write(pak.ToBuffer())
		log.Println("Sent channels")
	} else if packet.Command == 0x70a {
		// pakGetIP
		log.Println("get ip")

		// TODO: check if logged in

		var servernum = packet.Buffer[0]
		var channelnum = packet.Buffer[4]

		log.Printf("Servernum: %v - Channel: %v", servernum, channelnum)
		beginPacket(&pak, 0x70a)

		var channel Channel
		row := db.QueryRow("SELECT host,port,lanip,lansubmask FROM channels WHERE id=? and type=1", servernum)
		err := row.Scan(&channel.host, &channel.port, &channel.lanip, &channel.lansubmask)
		if err != nil {
			log.Println("Player selected channel that is invalid or offline")
			return conn.Close()
		}

		addByte(&pak, 0)
		addDWord(&pak, 1) // TODO: should be user ID
		addDWord(&pak, 0x87654321)

		localAddr := conn.LocalAddr().String()

		if localAddr == channel.lansubmask {
			log.Println("LAN")
			addString(&pak, channel.lanip)
		} else if localAddr == "127.0.0.1" {
			log.Println("Localhost")
			addString(&pak, "127.0.0.1")
		} else {
			log.Println("Other (External)")
			addString(&pak, channel.host)
		}

		addByte(&pak, 0)
		addWord(&pak, channel.port)
		conn.Write(pak.ToBuffer())
	} else {
		log.Println("Unknown packet:", fmt.Sprintf("Command: %#v", packet.Command))
	}

	return nil
}

func cryptPacket(buf *[]byte) {
	pakLen := int((*buf)[0]) - 2
	for i := 2; i < pakLen; i++ {
		(*buf)[i] = 0x61 ^ (*buf)[i]
	}
}

func main() {
	var err error
	db, err = sql.Open("mysql", "root:dont,forget,this@/gorose")
	if err != nil {
		log.Fatal("Could not connect to database:", err.Error())
	}
	// if err := db.PingContext(ctx); err != nil {
	// 	log.Fatal(err)
	// }
	defer db.Close()

	ln, err := net.Listen(ConnType, ConnHost+":"+ConnPort)
	if err != nil {
		log.Fatal("Error listening:", err.Error())
	}
	defer ln.Close()

	log.Println("Listening on " + ConnHost + ":" + ConnPort)
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println("Error listening:", err.Error())
			continue
		}

		log.Println("Client connected:", conn.RemoteAddr())
		go handleConnection(conn)
	}
}
