package main

import (
	"log"

	"gitlab.com/michaelowens/gorose/common"
)

// LoginServer booo
type LoginServer struct {
	common.SocketServer
}

func (s *LoginServer) onPacket(data []byte) {
	log.Println("LoginServer onPacket")
}

func main() {
	server := &LoginServer{}
	server.OnNewPacket(server.onPacket)
	server.Start()
}
