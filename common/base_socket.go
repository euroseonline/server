package common

import "net"

// BaseSocket struct
type BaseSocket struct {
	socket net.Listener
}
