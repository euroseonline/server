package common

import (
	"io"
	"log"
	"net"
	"time"
)

const (
	// ConnHost Host to bind to
	ConnHost = "127.0.0.1"
	// ConnPort Port to bind to
	ConnPort = "29000"
	// ConnType Type of connection
	ConnType = "tcp"
)

// SocketServerType a
type SocketServerType interface {
	onPacket()
}

// SocketServer Wrapper for each server handling sockets
type SocketServer struct {
	adapter          SocketServerType
	socket           net.Listener
	ConnectedClients uint32
	onNewPacket      func(data []byte)
}

// Start func
func (s *SocketServer) Start() {
	var err error
	s.socket, err = net.Listen(ConnType, ConnHost+":"+ConnPort)
	if err != nil {
		log.Fatal("Error listening:", err.Error())
	}
	defer s.socket.Close()

	log.Println("Listening on " + ConnHost + ":" + ConnPort)
	s.ServerLoop()
}

// OnNewPacket Add callback to call when new packet is received
func (s *SocketServer) OnNewPacket(callback func(data []byte)) {
	s.onNewPacket = callback
}

// ServerLoop Loop to accept new clients
func (s SocketServer) ServerLoop() {
	for {
		conn, err := s.socket.Accept()
		if err != nil {
			log.Println("Error listening:", err.Error())
			continue
		}

		log.Println("Client connected:", conn.RemoteAddr())
		go s.handleConnection(conn)
	}
}

func (s *SocketServer) handleConnection(conn net.Conn) {
	defer func() {
		log.Println("Closing connection...", conn.RemoteAddr())
		conn.Close()
	}()

	timeoutDuration := 1 * time.Second

	// Make a buffer to hold incoming data.
	buf := make([]byte, 1024)

	for {
		conn.SetReadDeadline(time.Now().Add(timeoutDuration))

		bufLen, err := conn.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
		}

		if bufLen < 1 {
			continue
		}

		log.Println("Received data of length:", bufLen)

		bufPointer := 0
		for bufPointer < bufLen {
			pakLen := int(buf[bufPointer])
			packet := buf
			log.Println("Received packet of length:", bufLen)
			log.Println("Packet:", packet)

			// cryptPacket(&packet)
			// log.Println("Packet:", packet)

			// p := Packet{binary.LittleEndian.Uint16(packet[0:2]), binary.LittleEndian.Uint16(packet[2:4]), binary.LittleEndian.Uint16(packet[4:6]), packet[6:]}

			// err := handlePacket(conn, p)
			// if err != nil {
			// 	log.Println("Packet was not handled?")
			// }

			// s.onPacket()
			s.onNewPacket(packet)

			bufPointer += pakLen
		}
		// log.Println(buf)
	}
}

func (s *SocketServer) onPacket() {}
