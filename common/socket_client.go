package common

import (
	"net"
)

// SocketClient Wrapper for each client socket
type SocketClient struct {
	id     uint16
	socket net.Conn
}

// NewSocketClient Initiate a new SocketClient
func NewSocketClient(socket net.Conn) *SocketClient {
	return &SocketClient{
		socket: socket,
	}
}
